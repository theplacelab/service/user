const { create } = require('../models/user.js');
const dbSchema = require('../schema.js');
const { createTable } = require('../util.js');

exports.up = function (knex) {
  return createTable(knex, 'users');
};

exports.down = function (knex) {
  return knex.schema.dropTable('users');
};
