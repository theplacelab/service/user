const { v4: uuidv4 } = require('uuid');
const { faker } = require('@faker-js/faker');
const { ROLE } = require('../../constants.js');
const fs = require('fs');
const path = require('path');
const os = require('os');
const tempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'auth-service'));
const database = `${tempDir}/${uuidv4()}.db`;
const jwt = require('jsonwebtoken');

const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: database
  },
  useNullAsDefault: true,
  migrations: {
    directory: __dirname + '/../../data/migrations'
  },
  seeds: {
    directory: __dirname + '/../../data/seeds'
  }
});

module.exports = {
  knex,
  generateUser: (props) => ({
    avatar: faker.internet.avatar(),
    name: faker.person.fullName(),
    email: faker.internet.email().toLowerCase(),
    password: faker.internet.password(),
    role: ROLE.USER,
    f_valid: 0,
    f_reset: 0,
    created_at: Date.now(),
    uuid: uuidv4(),
    ...props
  }),
  validTokenFor: (user) => {
    return jwt.sign(
      {
        ...user,
        email: user.email.toLowerCase(),
        exp: Math.floor(Date.now() / 1000) + 1 * 60
      },
      process.env.JWT_SECRET
    );
  },
  before: (done) => {
    process.env.JWT_SECRET = 'testsecret';
    done();
  },
  after: () => {
    return knex.migrate.rollback().then(() =>
      knex.destroy().then(() => {
        fs.unlink(database, () => {});
      })
    );
  }
};
