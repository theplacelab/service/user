const request = require('supertest');
const server = require('../../../app');
const { knex, before, after } = require('../common.js');
const { RESPONSE } = require('../../../constants.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const { error } = require('console');

beforeAll(before);
afterAll(after);

// Create
describe(`API: User Validation`, () => {
  test('ANON: Account can be reset on request', (done) => {
    const userEmail = testUsers[0].email.toLowerCase();
    server.start(
      () => {
        request(server.app)
          .post(`/user/reset`)
          .send({
            email: userEmail
          })
          .then((response) => {
            expect(response.statusCode).toEqual(RESPONSE.OK);
            request(server.app)
              .post(`/user/status`)
              .send({ email: userEmail })
              .then((response) => {
                const { email, f_reset, f_valid } = JSON.parse(response.text);
                expect(email).toBe(userEmail);
                expect(f_valid).toBeFalsy();
                expect(f_reset).toBeTruthy();
                done();
              });
          });
      },
      knex,
      true
    );
  });
});
