const request = require('supertest');
const { mock } = require('nodemailer');
const { ROLE, RESPONSE } = require('../../../constants.js');
const server = require('../../../app');
const { knex, before, after, generateUser } = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const series = require('async/series');

beforeAll(before);
afterAll(after);

describe(`API: User Validation`, () => {
  test('ANON: Validation flow.', (done) => {
    let validationToken;
    const testUser = { ...generateUser({ f_valid: 0, role: ROLE.SUPER }) };
    server.start(() => {
      series(
        [
          (cb) => {
            request(server.app)
              .put(`/user`)
              .send(testUser)
              .then((response) => {
                const { role, email, name, f_reset, f_valid } = JSON.parse(
                  response.text
                );
                expect(response.statusCode).toEqual(RESPONSE.CREATED);
                expect(email).toEqual(testUser.email);
                expect(name).toEqual(testUser.name);
                expect(role).toEqual(ROLE.USER);
                expect(f_reset).toBeTruthy();
                expect(f_valid).toBeFalsy();
                mock.onSend = () => {
                  // Get a validation email
                  const sentEmails = mock.getSentMail();
                  expect(sentEmails.length).toBe(1);
                  expect(sentEmails[0].to).toBe(email);
                  // Extract validation token from email
                  const myRegexp = new RegExp('href=".+/validate/(.+?)"', 'gm');
                  const match = myRegexp.exec(sentEmails[0].html);
                  validationToken = match[1];
                  cb();
                };
              });
          },
          (cb) => {
            request(server.app)
              .post(`/user/validate`)
              .send({ token: validationToken })
              .then((response) => {
                // Validate account
                expect(response.statusCode).toEqual(RESPONSE.OK);
                cb();
              });
          },
          (cb) => {
            request(server.app)
              .post(`/user/validate`)
              .send({ token: validationToken })
              .then((response) => {
                // Validation links are single use
                expect(response.statusCode).toEqual(410);
                cb();
              });
          },
          (cb) => {
            request(server.app)
              .post(`/user/status`)
              .send({ email: testUser.email })
              .then((response) => {
                const { email, f_reset, f_valid } = JSON.parse(response.text);
                expect(email).toBe(testUser.email);
                expect(f_valid).toBeTruthy();
                expect(f_reset).toBeTruthy();
                cb();
              });
          }
        ],
        done
      );
    }, knex);
  });
});
