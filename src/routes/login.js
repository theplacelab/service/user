const router = require('express').Router();
const user = require('../data/models/user');

router.post('/', (req, res) => {
  const { email, pass, extend } = req.body;
  user.login(req.knex, { email, pass, extend }, (response, code) =>
    res.status(code).send(response)
  );
});

module.exports = router;
