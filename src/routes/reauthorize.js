const router = require('express').Router();
const userActions = require('../data/models/user');
const { RESPONSE } = require('../constants');
const { requireValidRefreshToken } = require('../modules/validation');

router.post('/', (req, res) => {
  requireValidRefreshToken(req, res, async (user) => {
    const tokens = await userActions.issueTokens(req.knex, user, user.extend);
    res.status(RESPONSE.OK).send(tokens);
  });
});

module.exports = router;
