let jwt = require('jsonwebtoken');
const axios = require('axios');

const sanitize = (email) => {
  return email.toLowerCase().replaceAll('+', '%2b');
};

module.exports = {
  createUser: (data, cb) => {
    secureRequest('POST', `${process.env.USER_API_URL}/s_users`, data, cb);
  },
  readUser: (data, cb) => {
    secureRequest(
      'GET',
      `${process.env.USER_API_URL}/users?email=eq.${sanitize(data.email)}`,
      data,
      cb
    );
  },
  updateUser: (data, cb) => {
    secureRequest(
      'PATCH',
      `${process.env.USER_API_URL}/s_users?email=eq.${sanitize(data.email)}`,
      data,
      cb
    );
  },
  deleteUser: (data, cb) => {
    secureRequest(
      'DELETE',
      `${process.env.USER_API_URL}/users?email=eq.${sanitize(data.email)}`,
      null,
      cb
    );
  },
  consumeToken: (token, cb) => {
    console.log(`${process.env.USER_API_URL}/rpc/consumetoken`);
    secureRequest(
      'POST',
      `${process.env.USER_API_URL}/rpc/consumetoken`,
      { token },
      cb
    );
  }
};

const secureRequest = (method, url, data, cb) => {
  const options = {
    url,
    data,
    method: method,
    headers: {
      Authorization: `Bearer ${jwt.sign(
        {
          role: ROLE.SERVICE,
          exp: Math.floor(Date.now() / 1000) + 2 * 60 // 2 min token
        },
        `${process.env.JWT_SECRET}`
      )}`,
      'Content-Type': 'application/json; charset=utf-8'
    }
  };
  axios(options)
    .then((response) => cb(response, null))
    .catch((err) => cb(null, err));
};
